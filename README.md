
## NumPy
![alt text](https://xp.io/storage/1AWtCN6L.png)
![alt text](https://xp.io/storage/1AWdLcrZ.png)
NumPy is a library for the Python programming language, adding support for large, multi-dimensional arrays and matrices, along with a large collection of high-level mathematical functions to operate on these arrays. The ancestor of NumPy, Numeric, was originally created by Jim Hugunin with contributions from several other developers.
